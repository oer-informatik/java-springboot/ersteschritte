# Das SpringBoot-Framework installieren und die IDE konfigurieren
## Installationen und Voraussetzungen
### Eine aktuelle Java JDK
Wir benötigen das _Java Development Kit_, das Buildwerkzeug _Maven_, die Versionsverwaltung _git_ und eine Entwicklungsumgebung. Konkret werden folgende Versionen vorgeschlagen:
_Java JDK 11 LTS_ als aktuelle Java-Version (alternativ: JDK8 - JDK1x)
- Oracle LTS-JDK (Login erforderlich): [https://www.oracle.com/java/technologies/]()
- Alternativ: OpenJDK [http://jdk.java.net/]()

Nach der Installation müssen die Umgebungsvariablen gesetzt werden für `JAVA\_HOME` und `PATH`, Anleitung unter: [https://www.java.com/de/download/help/path.xml]()

Unter Debian/Linux:
```bash
$ sudo apt-get install openjdk-11-jdk openjdk-8-jre-headless openjfx
```

Welche Version wurde installiert und hat die Installation geklappt (im Terminal bzw. der PowerShell)?
```bash
$ java -version
```
```
    java version "11.0.4" 2019-07-16 LTS
    Java(TM) SE Runtime Environment 18.9 (build 11.0.4+10-LTS)
    Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.4+10-LTS, mixed mode)
```

(Sofern hier noch `java version "1.8.0_261"` oder eine Version \<11.0 gemeldet werden sollte muss überprüft werden, ob ein aktuelles Java installiert ist.)

### Eine SpringBoot-fähige IDE: Netbeans
Als Entwicklungsumgebung (IDE) empfiehlt sich _Netbeans_, die Java-Referenz IDE. Eine gute SpringBoot-Integration weisen Alternativ auch die _Spring Tool Suite_ (STS, basiert auf Eclipse) oder _IntelliJ IDEA_ auf, die Beispiele hier werden jedoch mit Netbeans umgesetzt.

Download / Installation unter:
[https://netbeans.apache.org/download/]()


### Ein Build-Tool: Maven
_Maven_  als Build-Werkzeug (alternativ: _Gradle_) wurde als Bestandteil von Netbeans bereits installiert, es muss lediglich der `PATH` eingetragen werden (s.o.):
	(liegt unter `./netbeans/java/maven/bin`)

Unter Debian/Linux:
```bash
$ sudo apt-get install maven
```

```bash
$ mvn -v
```

```
	Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-10T17:41:47+01:00)
	Maven home: C:\Program Files\netbeans\java\maven\bin\..
	Java version: 11.0.4, vendor: Oracle Corporation
	Java home: C:\Program Files\Java\jdk-11.0.4
	Default locale: de_DE, platform encoding: Cp1252
	OS name: "windows 10", version: "10.0", arch: "amd64", family: "dos"
```
### Versionskontrollsystem `git`
Download des Versionskontrollsystems _git_ unter  [https://git-scm.com/download/]()
Vor der Installation sollte ein Editor vorhanden sein (VSCode, Notepad++, Atom...), andernfalls wird _vim_ ausgewählt...

Es können alle Standardwerte übernommen werden - Infos siehe `git`-Infoblätter.

Unter Debian/Linux:
```bash
$ sudo apt-get install  git
```
Hat die Installation geklappt? Welche Version ist installiert?
```bash
$ git --version
```
```
    git version 2.23.0.windows.1
```

### Einrichtung von Netbeans
Netbeans starten

![](images/InstallNetbeans/01_StartNetBeans.png)


Ins Menü Tools/Plugin wechseln

![](images/InstallNetbeans/02_Tools-Plugins.png)



Registerkarte "Settings": Prüfen, ob die UpdateCenter aktiviert sind (bis auf Netbeans 8.2).

![](images/InstallNetbeans/03_CheckSettings.png)


Registerkarte "Updates" auf neue Updates prüfen

![](images/InstallNetbeans/04_CheckForUpdates.png)


Registerkarte "Available Plugins" Liste aktuallisieren ("Check for Newest")

![](images/InstallNetbeans/05_CheckForNewest.png)


Das Plugin "NB SpringBoot" auswählen und installieren (mittlerweile gibt es eine neuere Version als auf dem Screenshot)

![](images/InstallNetbeans/06_SpringBootWaehlen.png)


"Install" (unten) klicken

![](images/InstallNetbeans/07_InstallWaehlen.png)


Weiter klicken...

![](images/InstallNetbeans/08_Next.png)

Die Lizenzbedingungen lesen und akzeptieren
![](images/InstallNetbeans/09_LicenseAgreement.png)


Warten bis die Installation abgeschlossen ist.

![](images/InstallNetbeans/10_SaveInstall.png)


Zustimmen, dass unsichere Server genutzt werden können.

![](images/InstallNetbeans/11_InstallUntrustedPlugin.png)


Die IDE neu starten.

![](images/InstallNetbeans/12_RestartIDENow.png)

## Links und weitere Informationen
- [Das SpringBoot-Plugin von Netbeans](http://plugins.netbeans.org/plugin/67888/nb-springboot)

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/ersteschritte]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).
![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
