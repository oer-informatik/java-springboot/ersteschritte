# Hello SpringBoot - ein erstes Projekt
## Ein erstes Projekt erstellen

Nach der Installation von _NB SpringBoot_ findet sich unter File / New Project eine neue Rubrik

![](images/FirstProject/00_NewProject.png)


"Java with Maven" / "Spring Boot Initializr Project"

![](images/FirstProject/01_ChooseSpringBootInitializr.png)


Im Folgenden werden müssen eine Reihe von Projektdaten angegeben werden.
Wer nicht Netbeans nutzt kann sein Projekt auch unter [https://start.spring.io/]() mit einer identischen Maske erstellen.

![](images/FirstProject/02_BaseProperty.png)

```
Group:
Artifact:
Version:
```

![](images/FirstProject/02_BaseProperty_Group.png)


JAR: Ein mit Maven oder per Konsole ausführbares JAR-Archiv (auswählen)
WAR: Ein Java EE-Container, der z.B. mit Tomcat genutzt wird
Hintergrund z.B.: [https://www.baeldung.com/java-jar-war-packaging]()

![](images/FirstProject/02_BaseProperty_Packaging.png)


Name und Package Name sollten sich automatisch angepasst haben - eine Beschreibung kann noch ergänzt werden.

![](images/FirstProject/02_BaseProperty_Name.png)


Es stehen prinzipiell verschiedene JVM-Sprachen zur Auswahl

![](images/FirstProject/02_BaseProperty_Language.png)


Theoretisch ist SpringBoot 2 mit Java 8 möglich - sinnvoll ist es jetzt die aktuelle JDK11 LTS zu verwenden.

![](images/FirstProject/02_BaseProperty_JavaVersion.png)


Das Startprojekt soll als einzige Abhängigkeit "Spring Web" erhalten.


![](images/FirstProject/03_Dependencies_Web.png)


Hier einfach weiterklicken oder Namen anpassen...

![](images/FirstProject/04_NameAndLocation.png)

Es müssen erst allerlei Abhängigkeiten geladen werden, was man unten am Progressbar sieht. Es dauert eine Weile, bis das Projekt per Taste `F6` oder Menü "Run"/"Run" ausgeführt werden kann.

![](images/FirstProject/05_ResolveProblems_02.png)


Wenn das Projekt jetzt startet holt er die Dateien...

![](images/FirstProject/06_output_01.png)


... zeigt ein bisschen ASCII-Art...

![](images/FirstProject/06_output_02.png)


... und ist fertig...

![](images/FirstProject/06_output_03.png)

Aber weiter passiert erstmal nichts. Dazu gleich mehr.

## Was wurde erstellt?

Im Wesentlichen wurden zwei Dateien erstellt: eine Application-Class (`HelloSpringApplication.java`) und eine Konfigurations-Datei (`pom.xml`). In Netbeans kann man entweder über den `Projects` oder den `Files`-Tab navigieren, um sie zu finden:

Files:

![](images/FirstProject/07_Dateistruktur.png)


Projects:

![](images/FirstProject/07_Projektstruktur.png)


Die erstellten Dateien sehen etwa so aus:

```java
package de.csbme.HelloSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloSpringApplication.class, args);
	}

}
```

Das Programm ist im Prinzip startbar - bleibt aber ohne jede Funktion. Wir wollen das Programm so anpassen, als es als Antwort auf einen HTTP-Request mit `"Hello World!"` antwortet. Dazu müssen wir (in umgekehrter Reihenfolge):
- Eine Methode schreiben, die mit "Hello World!" antwortet.
- Diese Methode so bei SpringBoot anmelden, dass SpringBoot weiß, dass diese Methode für HTTP-Requests zuständig ist (per Annotation).
- Die nötigen Annotationen laden, die wir für obige Kennzeichnung benötigen.

Wir beginnen mit letzterem. In der Datei `HelloSpringApplication.java` muss:

Bei den beiden vorhandenen Imports muss ein weiterer ergänzt werden, der die nötigen Bibliotheken läd:
```java
import org.springframework.web.bind.annotation.*;
```

Die Annotation `@RestController`, die dem Framework mitteilt, dass diese Klasse regelt, wie auf HTTP-Request reagiert werden soll:
```java
@RestController
@SpringBootApplication
public class HelloSpringApplication {
```

Und eine Methode, die das Verhalten für den Fall festlegt, dass die Ressource "/" aufgerufen wird (also "http://localhost:8080/"). Hier kann natürlich auch jeder andere Pfad angegeben werden. `@RequestMapping("/hello")` wurde beispielsweise auf "http://localhost:8080/hello" reagieren. Der Port `:8080` ist bei SpringBoot voreingestellt, dazu später mehr.

```java
@RequestMapping("/")
String home() {
	return "Hello World! \n";
}
```

Das Programm kann jetzt über das Menü _Run_, die Taste `F6` oder das _Play_-Symbol gestartet werden.

![](images/FirstProject/06_run.png)

Im Browser sollte jetzt auf den Request "http://localhost:8080/" die korrekte Antwort erscheinen:

![](images/FirstProject/06_requestInBrowser.png)


Der HTTP-GET-Request auf der Ressource "localhost:8080/" (nichts anderes ist der Aufruf im Browser) sollte jetzt ein "Hello World!" als Antwort erhalten.
In einem Linux/MacOSX-Terminal mit installiertem `curl` lässt sich das ganze etwas besser testen, da wir mehr Informationen zurückgeliefert bekommen:
```bash
$ curl localhost:8080
    Hello World!
```


Um unter Windows über die PowerShell Get-Requests abzusetzen dient das Commandlet `Invoke-WebReques` (`curl` wird auch erkannt, ist hier ein Alias für CmdLets `Invoke-WebRequest` und muss entsprechend parametrisiert werden):
```PowerShell
> Invoke-WebRequest http://localhost:8080/
```
```
        StatusCode        : 200
        StatusDescription :
        Content           : Hello World!

        RawContent        : HTTP/1.1 200
        Content-Length: 14
        Content-Type: text/plain;charset=UTF-8
        Date: Sun, 22 Sep 2019 16:14:40 GMT

        Hello World!

        Forms             : {}
        Headers           : {[Content-Length, 14], [Content-Type, text/plain;charset=UTF-8], [Date, Sun, 22 Sep 2019 16:14:40
        	GMT]}
        Images            : {}
        InputFields       : {}
        Links             : {}
        ParsedHtml        : mshtml.HTMLDocumentClass
        RawContentLength  : 14
```

Sofern der Port 8080 bereits für andere Dienste benötigt wird lässt sich dieser Default-Port überschreiben, z.B. in der Datei `src/main/resources/application.properties` (wir werden später einige andere Möglichkeiten kennenlernen, die Konfiguration anzupassen):

![](images/FirstProject/09_NewPort.png)

```java
server.port=8085
```

Check: Klappt das mit dem neuen Port?
```bash
$ curl localhost:8085
```

```
        Hello World!
```

## Tests hinzufügen

Um unsere Application von Anfang an robust auszulegen benötigen wir noch Tests. Das Framework hat hierzu bereits eine jUnit-Testklasse vorbereitet, die wir nur noch mit Leben füllen müssen. Wir können über die Datei- oder über die Projektansicht zu den Testklassen navigieren:

Files:

![](images/FirstProject/08_TestFile.png)


Projects:

![](images/FirstProject/08_TestProject.png)


Als Voraussetzung für den Test benötigen wir ein Umfeld, in dem unsere HTTP-Requests aufgerufen werden. Hierzu benötigen wir ein Objekt der Klasse `WebApplicationContext`, das unsere App repräsentiert und ein Objekt der Klasse `MockMvc`, über das wir die HTTP-Aufrufe simulieren. Über die Annotation `@Autowired` weiß das SpringBoot-Framework, dass es selbst die zugehörigen Instanzen dieser Klassen verknüpfen muss.

```java
@Autowired
private WebApplicationContext webApplicationContext;
private MockMvc mockMvc;
```

Die Tests können erst durchgeführt werden, wenn das Mockobjekt instanziiert ist. Um das zu garantieren nutzt die Methode `setUp()` die _jUnit5_-Annotation `@BeforeEach` (diese Methode wird also vor allen Tests ausgeführt):

```java
    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
```

(Hinweis: falls Sie auf alte Beispiele stoßen, die mit `@Before` annotiert sind, so handelt es sich um jUnit4-Tests.)


Wir ergänzen zwei Tests: einen, der überprüft, ob der HTTP-StatusCode 200 (Ok) zurückgegeben wird und einen zweiten, der den Inhalt der Antwort überprüft:

```java
@Test
 public void httpStatusIsOK() throws Exception {
     ResultMatcher expected = MockMvcResultMatchers.status().isOk();
     mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(expected);
 }

 @Test
 public void contentLoads() throws Exception {
     ResultMatcher expected = MockMvcResultMatchers.content().string("Hello World! \n");
     mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(expected);

 }
```

Was fehlt sind noch die nötigen Imports. Die meisten fehlenden Imports findet Netbeans automatisch und schlägt sie im Kontextmenü der Hinweise (Glühlampensymbol an den Zeilen) direkt vor:

![](images/FirstProject/12_JavaDoc_und_Vorschläge_für_Imports.png)

Zwei Hinweise hierzu: Falls für `@Test` der Import `org.junit.Test` vorgeschlagen wird ist wohl noch jUnit4 installiert. Aktuell müsste die jUnit5-Variante `org.junit.jupiter.api.Test` angeboten werden. Bei der Annotation `@BeforeEach` hat Netbeans im meinem Fall Probleme, den richtigen Import zu finden: hier musste ich händisch eingeben:

```java
import org.junit.jupiter.api.BeforeEach;
```

Im Ganzen sieht es so aus:

```java
package de.csbme.HelloSpringBoot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
class HelloSpringBootApplicationTests {

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void httpStatusIsOK() throws Exception {
        ResultMatcher expected = MockMvcResultMatchers.status().isOk();
        mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(expected);
    }

    @Test
    public void contentLoads() throws Exception {
        ResultMatcher expected = MockMvcResultMatchers.content().string("Hello World! \n");
        mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(expected);

    }
}

```

Jetzt können die Tests ausgeführt werden: in Netbeans über das Menü _Run => Test Project_ oder die Taste `Alt-F6`.

![](images/FirstProject/10_ExecuteTests.png)


Im unteren Bereich der IDE sollte jetzt der Reiter "Test Results" verfügbar sein, in dem idealerweise "Both tests passed" steht:

![](images/FirstProject/11_TestResults.png)

## Links und weitere Informationen
- [Das SpringBoot-Plugin von Netbeans](http://plugins.netbeans.org/plugin/67888/nb-springboot)

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/ersteschritte]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).
![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
