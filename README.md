# SpringBoot

* [Spring Boot installieren](https://oer-informatik.gitlab.io/java-springboot/ersteschritte/docs/SpringBootInstallieren.html) [(als PDF)](https://oer-informatik.gitlab.io/java-springboot/ersteschritte/docs/SpringBootInstallieren.pdf)
* [Erstes SpringBoot Projekt](https://oer-informatik.gitlab.io/java-springboot/ersteschritte/docs/ErstesSpringBootProjekt.html) [(als PDF)](https://oer-informatik.gitlab.io/java-springboot/ersteschritte/docs/ErstesSpringBootProjekt.pdf)

# Infos zur genutzten gitlab CI und deren Nachnutzung:
Die HTML und PDF-Dateien wurden mit Hilfe der Konfiguraiton der [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab erstellt.
Die Vorlage findet sich hier: [https://gitlab.com/TIBHannover/oer/course-metadata-test/](https://gitlab.com/TIBHannover/oer/course-metadata-test/).
